package cc.camconn.bleh;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.UUID;

import static cc.camconn.bleh.Messenger.msgColor;

/**
 * Created by cam on 6/30/15.
 */
public class ChatEvents implements Listener {

    private Bleh plugin;

    public ChatEvents(Bleh plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onChatEvent(final AsyncPlayerChatEvent event) {
        String privateReason = ""; // reason to tell player they are kicked
        String kickReason; // what to publicly tell server
        String reason = "";
        if (event.getMessage().equalsIgnoreCase("banmeplz")) {
            privateReason = this.plugin.getConfig().getString("bleh.banmeplz");
            reason = "asking to be banned.";
        } else if (event.getMessage().equalsIgnoreCase("adminpls")) {
            privateReason = this.plugin.getConfig().getString("bleh.adminpls");
            reason = "thinking they would ever get admin.";
        } else if (event.getMessage().equalsIgnoreCase("modpls")) {
            privateReason = this.plugin.getConfig().getString("bleh.modpls");
            reason = "thinking they would ever get mod.";
        }

        if (reason.length() != 0) {
            kickReason = "Player " + event.getPlayer().getName() + " was kicked for " + reason;
            new AsyncKick(event.getPlayer().getUniqueId(), kickReason, privateReason).runTask(this.plugin);
        }

    }
}

class AsyncKick extends BukkitRunnable {
    private final UUID player;
    private final String kickReason;
    private final String publicReason;

    /**
     * Class to show kick message after player has been kicked and after player has chatted
     * @param player - The name of the player to be kicked
     * @param publicReason - The message to publicly show why the player was kicked.
     * @param privateReason - The message to tell the player why they were kicked.
     */
    public AsyncKick(UUID player, String publicReason, String privateReason) {
        this.player = player;
        this.kickReason = privateReason;
        this.publicReason = publicReason;
    }

    public void run() {
        Bukkit.getServer().getPlayer(this.player).kickPlayer(msgColor(kickReason));
        Bukkit.getServer().broadcastMessage(Messenger.msgColor(publicReason));
    }
}

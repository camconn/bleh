# bleh Plugin
A simple Spigot plugin that lets you ride the rails of minecraft as well as
have other fun with rails. There are also a few easter eggs.

The source code of this project is available on [Gitlab](https://gitlab.com/camconn/bleh).

# Summary
This repository contains the code for the *bleh* Minecraft/Spigot plugin. This
plugin is primarily meant for use with a private Spigot that the author
(Cameron Conn) runs, but the code has been made freely available for the public
benefit.

# Commands
- `/riderails [player] [locked]` - **r**ide some **r**ails 
- `/eject [player]` - eject from minecart
- `/rr` alias of `/riderails`

# Permissions
- `bleh.riderails` - spawn a minecart and ride some rails.
- `bleh.riderails.other` - make another play ride some rails 
- `bleh.eject` - eject yourself from a minecart
- `bleh.eject.other` - eject another player from a minecart

# Configuration
Configuration for bleh is located in `<spigot>/plugins/bleh/config.yml`.

`RideRails.Search-Radius` is the radius in blocks to search for rails from a
player's location, plus their position block. So a radius of 2 will search a
5x5 space near a player. The rest of the configuration is customizable messages.

Example `config.yml`:
```yml
# Configuration for Railways using /riderail and /eject
# If the string starts with a color code, you must enclose the string with quotes
# Example: "&cHang on tight!"
RideRails:
  Search-Radius: 2
  Leave: Thanks for riding!
  Locked: You're locked in, so you have to wait until the minecart is stopped, or use &e/eject&r to get out.
  Exit-Deny: You can't get out yet! Wait until the minecarts has stopped, or use &e/eject&r.
  Fail-Destination: Failed to parse target teleport location.
  Arrive-Destination: "&b&obleh Railways&r welcomes you to &4{DESTINATION}&r!"
  Eject-Failure: Sorry, but there was a problem ejecting you from the minecart :-(
  Launch: "&cHang on tight!"
bleh:
  modpls: No. Except on the Midnight of a Leap Day of a Leap Year. Even then, it's a 50-50.
  banmeplz: You said you wanted to be banned... &nRight?
  adminpls: The answer is &oalways&r "&lNO!"
``` 

# License
You are free to distribute this project, its code, and any products thereof
under the terms of the GNU Lesser Public License, Version 3 or any later
version. the full terms of this license are located in `LICENSE.txt` in the
project's root source code directory.

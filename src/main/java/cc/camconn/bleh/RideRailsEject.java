package cc.camconn.bleh;

import com.bergerkiller.bukkit.common.entity.CommonEntity;
import com.bergerkiller.bukkit.common.entity.type.CommonMinecart;
import com.bergerkiller.bukkit.common.entity.type.CommonMinecartRideable;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.BlockState;
import org.bukkit.block.Sign;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Minecart;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.entity.minecart.RideableMinecart;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.vehicle.VehicleBlockCollisionEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.Collection;
import java.util.HashSet;

import static cc.camconn.bleh.Messenger.*;

/**
 * Handles VehicleExitEvent as well as /eject
 */
public final class RideRailsEject implements Listener, CommandExecutor {
    /*
     * Force eject an player from a locked minecart.
     * NOTE: Player **must** be riding a minecart for this to work!
     *
     * @param Player - The player to eject from minecart
     */
    private Bleh plugin;
    private int searchRadius;

    public RideRailsEject(Bleh plugin) {
        this.plugin = plugin;
        this.searchRadius = this.plugin.getConfig().getInt("RideRails.Search-Radius");
    }

    /**
     * Eject an entity from a minecart, whether it's locked or not.
     * @param ejectee The Entity to eject
     * @return Whether or not the Entitiy was ejected successfully
     */
    private boolean ejectFromMinecart(Entity ejectee) {
        //eject player
        Minecart mc = (Minecart)ejectee.getVehicle();
        if (mc == null) return true;

        FixedMetadataValue ejectedMetadata = new FixedMetadataValue(this.plugin, true);
        mc.setMetadata("riderails-ejected", ejectedMetadata);

        if (mc.hasMetadata("teleporting")) {
            return true;
        }

        boolean mcEject = mc.eject();
        boolean isEjected = ejectee.isInsideVehicle();
        mc.removePassenger(ejectee);
        mc.remove();

        return (mcEject && isEjected && mc.isDead());
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player target = null;

        // determine what our target is
        if (sender instanceof Player) {
            if (args.length == 0) {
                target = (Player) sender;
                if (!sender.hasPermission("bleh.riderails.eject")) {
                    sender.sendMessage(command.getPermissionMessage());
                    return true;
                }
            } else {
                if (!sender.hasPermission("bleh.riderails.eject.other")) {
                    sender.sendMessage(command.getPermissionMessage());
                    return true;
                }
                Collection<? extends Player> players = plugin.getServer().matchPlayer(args[0]);
                for (Player p : players) {
                    target = p;
                    break;
                }

            }
        } else {
            if (args.length == 0) {
                sender.sendMessage(pluginError("You must specify a player to eject."));
                return false;
            } else {
                target = plugin.getServer().getPlayer(args[0]);
                if (target == null) {
                    sender.sendMessage(pluginError("Player \"" + args[0] + "\" is not online, so I can't eject them."));
                    return true;
                }
            }
        }

        if (target == null) return false;

        // is target actually riding a minecart?
        if (!(target.getVehicle() instanceof Minecart)) {
            sender.sendMessage(pluginError("Player is not currently riding a minecart!"));
            return true;
        }

        // actually eject them
        if (ejectFromMinecart(target) ) {

            if (!(sender instanceof Player )|| !(sender).getName().equals(target.getName())) {
                sender.sendMessage(pluginMsg("Ejected player " + target.getName()));
                target.sendMessage(pluginMsg("You were ejected by " + sender.getName()));
            }
            target.sendMessage(pluginMsg(plugin.getConfig().getString("RideRails.Leave")));

        } else {
            sender.sendMessage(pluginError("Unfortunately, an error occurred :/"));
        }

        // no syntax error if we get this far
        return true;

    }

    /**
     * Handle exiting of RideRails minecarts. Cancel exit if minecart is locked. If not, remove
     * minecart whenever player exits.
     * @param event the VehicleExitEvent from Bukkit
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void onVehicleExit(VehicleExitEvent event) {
        if (event.getExited() instanceof Player && event.getVehicle() instanceof Minecart) {
            Minecart minecart = (Minecart) event.getVehicle();

            if (minecart == null)
                return;

            if (minecart.hasMetadata("riderails") && !minecart.hasMetadata("riderails-ejected")) {

                if (minecart.hasMetadata("stayseated")) {  // if we're forcing the player to stay seated
                    if (minecart.getVelocity().length() <= 0.000001) { // floating point hack

                        minecart.remove();
                    } else { // trying to get out of a minecart that's locked
                        event.getExited().sendMessage(pluginMsg(plugin.getConfig().getString("RideRails.Exit-Deny")));
                        event.setCancelled(true);
                        return;
                    }
                } else {
                    event.getVehicle().remove();
                    //ejectFromMinecart(event.getVehicle().getPassengers().get(0));
                }
                event.getExited().sendMessage(pluginMsg(plugin.getConfig().getString("RideRails.Leave")));
            }
        }

        // do nothing otherwise
    }

    /**
     * Handle event for player teleport to automatically eject them from the damn minecart. (This is a bugfix)
     * @param event the PlayerTeleportEvent from bukkit
     *
     */
    @EventHandler
    public void onPlayerTeleport(PlayerTeleportEvent event) {
        Entity e = event.getPlayer().getVehicle();
        if (!(e instanceof Minecart)) return;

        Minecart minecart = (Minecart) e;
        if (minecart != null && (minecart.hasMetadata("riderails") && !minecart.hasMetadata("riderails-ejected"))) {
            if (!minecart.hasMetadata("teleporting"))
                minecart.remove();
        }
    }

    /**
     * Handle end of rails collisions with [RailsEnd] signs. Eject use and optionally teleport user.
     * Also handles [RailsLaunch] signs.
     * @param event the VehicleBlockCollisionEvent object from Bukkit
     */
    @EventHandler(priority = EventPriority.NORMAL)
    public void onVehicleBlockCollision(VehicleBlockCollisionEvent event) {
        if (event.getVehicle() instanceof Minecart) {
            this.plugin.getLogger().info("It's a minecart");

            HashSet<Material> signBlocks = new HashSet<Material>();
            signBlocks.add(Material.SIGN);
            signBlocks.add(Material.WALL_SIGN);
            signBlocks.add(Material.SIGN_POST);

            Location signLoc = RideRail.findClosestBlock(event.getBlock().getLocation().clone(),
                    signBlocks, this.searchRadius);

            /*this.plugin.getLogger().info("signloc: " + signLoc);*/

            // If we've got nothing, do nothing
            if (signLoc == null) {
                this.plugin.getLogger().info("Could not find a sign!");
                return;
            }

            // We've got the sign, now let's get info about what to do
            Sign sign = (Sign) signLoc.getBlock().getState(); //always use getState, because otherwise Bukkit breaks
            String[] lines = sign.getLines();

            /*this.plugin.getLogger().info("sign lines:");
            this.plugin.getLogger().info(sign.getLine(0));
            this.plugin.getLogger().info(sign.getLine(1));
            this.plugin.getLogger().info(sign.getLine(2));*/
            Minecart m = (Minecart)event.getVehicle();

            if (lines[0].equalsIgnoreCase("[RailsEnd]")) {
                Entity rider = m.getPassengers().get(0);
                String destinationName = sign.getLine(1);
                String destinationLocationLine = sign.getLine(2);
                Location destination = null;

                destinationLocationLine = destinationLocationLine.replaceAll("[\\[\\],\\(\\)]", "");
                String[] coords = destinationLocationLine.split(" ");

                switch (coords.length) {
                    case 3:
                        try {
                            //preserve rider pitch and yaw to prevent disorientation
                            Location riderLoc = rider.getLocation();
                            /*destination = new Location(sign.getWorld(), Double.parseDouble(coords[0]),
                                    Double.parseDouble(coords[1]), Double.parseDouble(coords[2]),
                                    riderLoc.getYaw(), riderLoc.getPitch());*/
                            this.plugin.getLogger().info("x: " + coords[0] + ", y: " + coords[1] + ", z: " + coords[2]);

                            destination = new Location(sign.getWorld(), Integer.parseInt(coords[0].trim()),
                                    Integer.parseInt(coords[1].trim()), Integer.parseInt(coords[2].trim()),
                                    riderLoc.getYaw(), riderLoc.getPitch());
                        } catch (NumberFormatException failure) {
                            rider.sendMessage(pluginError(plugin.getConfig().getString("RideRails.Fail-Destination")));
                        }
                        break;
                    default:
                        //plugin.getLogger().info("coords.length: " + coords.length);;
                        break;
                }

                // now eject rider & teleport
                boolean isEjected = ejectFromMinecart(rider);
                if (isEjected) {
                    // teleport player if we have a location
                    if (destination != null)
                        rider.teleport(destination);

                    rider.sendMessage(pluginMsg(plugin.getConfig().getString("RideRails.Leave")));

                    // send nice welcome to {DESTINATION} message
                    if (!destinationName.isEmpty())
                        rider.sendMessage(msgColor(plugin.getConfig()
                                .getString("RideRails.Arrive-Destination")
                                .replace("{DESTINATION}", destinationName)));
                } else {
                    rider.teleport(destination.clone());
                    //rider.sendMessage(pluginError(plugin.getConfig().getString("RideRails.Eject-Failure")));
                }
            } else if (lines[0].equalsIgnoreCase("[RailsLaunch]")) {
                String directionLine = lines[1];
                directionLine = directionLine.replaceAll("[\\[\\],\\(\\)]", "");
                String[] vectorComponents = directionLine.split(" ");

                //.getLogger().info("Prexisting velocity: " + minecart.getVelocity().length());

                if (vectorComponents.length == 3) {
                    // for now, just launch us straight up :^)
                    // for now, we just go straight up
                    double xComponent = Double.parseDouble(vectorComponents[0]);
                    double yComponent = Double.parseDouble(vectorComponents[1]);
                    double zComponent = Double.parseDouble(vectorComponents[2]);

                    if (xComponent == 0 && yComponent == 0 && zComponent == 0) {
                        //plugin.getLogger().info("Couldn't parse new velocity components.");
                        return;
                    }

                    // teleport minecart half a block above the one it collided with so that it can go where it needs to
                    Location mcLoc = m.getLocation().clone();

                    // Add 0.5 in x and y to center block. Also, we're moved above the block we collided with to
                    // avoid further collisions
                    Location launchLoc = new Location(mcLoc.getWorld(), mcLoc.getBlockX() + 0.5,
                            event.getBlock().getLocation().getBlockY() + 2, mcLoc.getBlockZ() + 0.5);

                    Vector newVelocity = new Vector(xComponent, yComponent, zComponent);

                    Entity rider = m.getPassengers().get(0);
                    if (rider != null) {
                        rider.sendMessage(msgColor(this.plugin.getConfig().getString("RideRails.Launch")));
                        //rider.sendMessage("Launch speed: " + newVelocity.length());

                        //launchLoc.setYaw(rider.getLocation().getYaw());
                        //launchLoc.setPitch(rider.getLocation().getPitch());
                    }

                    // Instead of just using the teleport() method on the minecart, we need to use BKCommonLib to modify
                    // the location of the minecart *WHILE* keeping the player inside. Sounds great!
                    CommonEntity unsafeMinecart = CommonEntity.get(m);
                    if (unsafeMinecart instanceof CommonMinecart) {
                        //this.plugin.getLogger().info("Flying velocity mod: " +
                        //        ((CommonMinecartRideable) unsafeMinecart).getFlyingVelocityMod());
                        CommonMinecart mcr = (CommonMinecart) unsafeMinecart;
                        mcr.setFlyingVelocityMod(new Vector(1, 1, 1));
                        mcr.setMaxSpeed(1.0D);
                        mcr.setFootLocation(launchLoc.getX(), launchLoc.getY(), launchLoc.getZ(), launchLoc.getYaw(), launchLoc.getPitch());
                    }

                    // See DelayedLaunch for why we do this on a different tick.
                    //new DelayedLaunch(m, newVelocity).runTask(this.plugin);
                    new DelayedLaunch(m, newVelocity).runTaskLater(this.plugin, 1);
                } else {
                    // wasn't able to figure out where to go :(
                    plugin.getLogger().info("Couldn't find vectors to use!");
                }
            }
        }
    }
}


/**
 * This class is used because there's a shortcoming in Bukkit/Spigot/Minecraft's ability to be able to correctly
 * handle a vehicle if it is teleported and has it's velocity modified on the same tick. To fix this, we instead
 * teleport then modify the velocity (this class modifies the velocity a tick later).
 */
class DelayedLaunch extends BukkitRunnable {
    private Minecart minecart;
    private Vector launchVelocity;
    private float pitch, yaw;

    public DelayedLaunch(Minecart minecart, Vector launchVelocity) {
        this.minecart = minecart;
        this.launchVelocity = launchVelocity;
    }

    public void run() {
        //this.minecart.setVelocity(this.launchVelocity);  // use CommonEntity instead, buddy!
        CommonEntity<Minecart> mc = CommonEntity.get(this.minecart);
        mc.setVelocity(this.launchVelocity);;
    }
}

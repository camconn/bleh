package cc.camconn.bleh;

/**
 * Created by cam on 6/27/15.
 */

import org.bukkit.ChatColor;

public class Messenger {
    private static final String prefix = "[bleh]";  // default plugin prefix to use

    /**
     * Standard color code formatting wrapper with no plugin prefix
     * @param msg The String to translate
     * @return A color-code formatted string
     */
    public static String msgColor(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
    /**
     * Standard Plugin Message Formatting
     * @param msg The String to format
     * @return A formatted string with the plugin prefix
     */
    public static String pluginMsg(String msg) {
        return pluginMsg(Messenger.prefix, msg);
    }

    /**
     * Standard plugin message formatting
     * @param prefix The prefix to add to the message
     * @param msg The message to send
     * @return - A formatted message with the plugin prefix
     */
    public static String pluginMsg(String prefix, String msg) {
        return ChatColor.AQUA + prefix + ChatColor.RESET + " " + ChatColor.translateAlternateColorCodes('&', msg);
    }

    /**
     * Format an error message with the plugin name out front
     * @param msg The message to send
     * @return A color-code formatted error message with the plugin prefix
     */
    public static String pluginError(String msg) {
        return pluginMsg(Messenger.prefix, "&4" + msg);
    }

}

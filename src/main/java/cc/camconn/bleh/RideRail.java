
package cc.camconn.bleh;

import com.bergerkiller.bukkit.common.entity.CommonEntity;
import com.bergerkiller.bukkit.common.utils.BlockUtil;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Vehicle;
import org.bukkit.material.Rails;
import org.bukkit.metadata.FixedMetadataValue;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import static cc.camconn.bleh.Messenger.*;


/**
 * /RideRail command
 */
public class RideRail implements CommandExecutor {
    private int searchRadius;
    private cc.camconn.bleh.Bleh bleh;

    /**
     * Create a RideRail instance
     * @param plugin - The bleh plugin for reference.
     */
    public RideRail(cc.camconn.bleh.Bleh plugin) {
        this.searchRadius = plugin.getConfig().getInt("RideRails.Search-Radius");
        this.bleh = plugin;
    }

    /**
     * Find the closest block to a location
     * @param loc - Location of where to search for rails
     * @param blockTypes - a Set of blocks to search for
     * @param radius - The radius to search for blocks in
     * @return Closest rail's Location or null
     */
    public static Location findClosestBlock(Location loc, Set<Material> blockTypes, int radius) {
        World world = loc.getWorld();
        double smallestDistance = -1;
        Location closestBlock = null;

        for (int x = loc.getBlockX() - radius; x <= loc.getBlockX() + radius; x++) {
            for (int y = loc.getBlockY() - radius; y <= loc.getBlockY() + radius; y++) {
                for (int z = loc.getBlockZ() - radius; z <= loc.getBlockZ() + radius; z++) {
                    Location targetLocation = new Location(world, x, y, z);
                    Block b = world.getBlockAt(targetLocation);

                    if (blockTypes.contains(b.getState().getType())) {
                        double distance = loc.distance(targetLocation);

                        if (smallestDistance == -1 || distance < smallestDistance)
                            smallestDistance = distance;
                            closestBlock = targetLocation;
                    }
                }
            }
        }

        if (closestBlock != null) {
            closestBlock.setYaw(loc.getYaw());
            closestBlock.setPitch(loc.getPitch());
            closestBlock.setWorld(world);
        }

        return closestBlock;
    }

    private Location findClosestBlock(Location loc, Set<Material> blockTypes) {
        return findClosestBlock(loc, blockTypes, this.searchRadius);
    }

    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        boolean isLocked = false;
        Player target = null;

        if (label.equalsIgnoreCase("rrlock"))
            isLocked = true;

        // parse command
        if (args.length == 2) {
            if (args[1].equalsIgnoreCase("locked")) {
                isLocked = true;
            } else {
                sender.sendMessage(pluginError("Invalid syntax."));
                return false;
            }
            target = bleh.getServer().getPlayer(args[0]);
        } else if (args.length == 1) {
            if (args[0].equalsIgnoreCase("locked")) {
                target = (Player) sender;
                isLocked = true;
            } else {
                target = bleh.getServer().getPlayer(args[0]);
            }
        } else if (args.length == 0) {
            if (sender instanceof Player) {
                target = (Player) sender;
            } else {
                sender.sendMessage(pluginError("You must specify a player for that command!"));
                return false;
            }
        }

        // check target is online (sanity check)
        if (target == null) {
            sender.sendMessage(pluginError("Player " + args[0] + " is not online!"));
            return true;
        }

        // check for permissions
        if (sender instanceof Player) {
            if (((Player) sender).getName().equalsIgnoreCase(target.getName())) {
                if (!sender.hasPermission("bleh.riderails")) {
                    sender.sendMessage(command.getPermissionMessage());
                    return true;
                }
            } else if (!sender.hasPermission("bleh.riderails.other")) {
                    sender.sendMessage(command.getPermissionMessage());
                    return true;
            }
        }

        /*
         * We now know that the target player is online, and that the sender has permissions, now it's time
         * to actually do stuff!
         */
        Location playerLoc = target.getLocation();
        if (playerLoc == null)
            return true;

        HashSet<Material> railTypes = new HashSet<Material>();
        railTypes.add(Material.RAILS);
        railTypes.add(Material.POWERED_RAIL);
        railTypes.add(Material.DETECTOR_RAIL);
        railTypes.add(Material.ACTIVATOR_RAIL);

        Location nearestRailLoc = findClosestBlock(playerLoc.clone(), railTypes);
        if (nearestRailLoc == null) {
            sender.sendMessage(pluginError("No rails found within " + this.searchRadius + " blocks."));
            return true;
        }

        Rails nearestRail = BlockUtil.getRails(nearestRailLoc.getBlock());
        if (nearestRail == null) {
            // We goofed. Abort everything
            this.bleh.getLogger().log(Level.WARNING, pluginError("We found the nearest rail, but it wasn't a rail!"));
            return false;
        }

        nearestRailLoc.add(0.5, 0.35, 0.5);
        if (nearestRail.isOnSlope()) {
            nearestRailLoc.add(0, 0.5, 0);
        }

        Entity spawnedMinecart = playerLoc.getWorld().spawnEntity(nearestRailLoc, EntityType.MINECART);

        FixedMetadataValue tpMetadata = new FixedMetadataValue(bleh, true);
        spawnedMinecart.setMetadata("teleporting", tpMetadata);

        FixedMetadataValue rideRailMetadata = new FixedMetadataValue(bleh, true);
        spawnedMinecart.setMetadata("riderails", rideRailMetadata);

        // add locking metadata
        if (isLocked) {
            FixedMetadataValue lockedMetadata = new FixedMetadataValue(bleh, true);
            spawnedMinecart.setMetadata("stayseated", lockedMetadata);
        }

        //target.teleport(nearestRailLoc);
        Vehicle spawnedVehicle = (Vehicle) spawnedMinecart;

        spawnedVehicle.setPassenger(target);
        target.sendMessage(pluginMsg("Enjoy your journey!"));

        if (isLocked) {
            target.sendMessage(pluginMsg(bleh.getConfig().getString("RideRails.Locked")));
        }

        spawnedMinecart.removeMetadata("teleporting", bleh);
        return true;
    }
}


/**
 * Main loader class
 */

package cc.camconn.bleh;

import org.bukkit.plugin.java.JavaPlugin;


public final class Bleh extends JavaPlugin {

    @Override
    public void onEnable() {
        loadConfiguration();

        this.getCommand("riderails").setExecutor(new RideRail(this));
        this.getCommand("eject").setExecutor(new RideRailsEject(this));

        //register event listeners
        this.getServer().getPluginManager().registerEvents(new RideRailsEject(this), this);
        this.getServer().getPluginManager().registerEvents(new ChatEvents(this), this); //broke
    }

    @Override
    public void onDisable() {
        //disable shit
        getLogger().info("Disabling plugin...");
    }

    private void loadConfiguration() {
        getConfig().options().copyHeader(true);
        getConfig().options().copyDefaults(true);
        saveConfig();

        getLogger().info("Default RideRail rail search radius is " +
            getConfig().getInt("RideRails.Search-Radius") + " blocks");
    }
}
